package aoc

import (
	"bufio"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

// CheckErr exits if the error is not nil
func CheckErr(err error) {
	if err != nil {
		log.Fatalf("%v", err)
	}
}

// ReadFile reads a file into a string
func ReadFile(filename string) (string, error) {
	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		return "", err
	}

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

// ReadAllLines reads all lines from a file
func ReadAllLines(filename string) ([]string, error) {
	lines := []string{}

	file, err := os.Open(filename)
	defer file.Close()
	if err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}

// StringsToInts converts strings to ints
func StringsToInts(strings []string) ([]int, error) {
	ints := []int{}
	for _, s := range strings {
		i, err := strconv.Atoi(s)
		if err != nil {
			return nil, err
		}
		ints = append(ints, i)
	}
	return ints, nil
}
