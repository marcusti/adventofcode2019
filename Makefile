PKGS := $(shell go list ./... | grep -v /vendor/)
PROJECT := gitlab.com/marcusti/adventofcode2019

.PHONY: test
test:
	go mod tidy
	go mod vendor
	go fmt $(PKGS)
	go vet $(PKGS)
	go test $(PKGS)
	cd cmd/day01 ; go run main.go
	cd cmd/day02 ; go run main.go
