package main

import (
	"log"

	"gitlab.com/marcusti/adventofcode2019/aoc"
)

type solve func(int) int

func main() {
	lines, err := aoc.ReadAllLines("data.txt")
	aoc.CheckErr(err)

	ints, err := aoc.StringsToInts(lines)
	aoc.CheckErr(err)

	run(ints, part1, "day 01-1")
	run(ints, part2, "day 01-2")
}

func run(ints []int, fn solve, name string) {
	sum := 0
	for _, i := range ints {
		sum += fn(i)
	}
	log.Printf("%s: %d", name, sum)
}

func part1(i int) int {
	return i/3 - 2
}

func part2(i int) int {
	sum := 0
	current := i
	for {
		r := part1(current)
		if r >= 0 {
			sum += r
			current = r
		} else {
			break
		}
	}
	return sum
}
