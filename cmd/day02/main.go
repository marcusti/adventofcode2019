package main

import (
	"encoding/csv"
	"log"
	"os"

	"gitlab.com/marcusti/adventofcode2019/aoc"
)

type solve func(int) int

func main() {
	runPart1(readInput(), "day 02-1")

	i, j := runPart2()
	log.Printf("day 02-2: %d", (100*i)+j)
}

func readInput() []int {
	f, err := os.Open("data.txt")
	defer f.Close()
	aoc.CheckErr(err)

	r := csv.NewReader(f)
	record, err := r.Read()
	aoc.CheckErr(err)

	ints, err := aoc.StringsToInts(record)
	aoc.CheckErr(err)

	return ints
}

func runPart1(ints []int, name string) {
	ints[1] = 12
	ints[2] = 2
	endState := process(ints)
	log.Printf("%s: %v", name, endState[0])
}

func runPart2() (int, int) {
	for i := 0; i <= 99; i++ {
		for j := 0; j <= 99; j++ {
			ints := readInput()
			ints[1] = i
			ints[2] = j
			endState := process(ints)
			if endState[0] == 19690720 {
				return i, j
			}
		}
	}
	return -1, -1
}

func process(ints []int) []int {
	i := 0
	for {
		if ints[i] == 99 {
			break
		}
		opcode := ints[i]
		x := ints[i+1]
		y := ints[i+2]
		z := ints[i+3]
		valueAtX := ints[x]
		valueAtY := ints[y]
		if opcode == 1 {
			ints[z] = valueAtX + valueAtY
		}
		if opcode == 2 {
			ints[z] = valueAtX * valueAtY
		}
		i += 4
	}
	return ints
}
