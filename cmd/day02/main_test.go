package main

import (
	"reflect"
	"testing"
)

type intslice *[]int

var (
	data1 = map[intslice]intslice{
		&[]int{1, 0, 0, 0, 99}:              &[]int{2, 0, 0, 0, 99},
		&[]int{2, 3, 0, 3, 99}:              &[]int{2, 3, 0, 6, 99},
		&[]int{2, 4, 4, 5, 99, 0}:           &[]int{2, 4, 4, 5, 99, 9801},
		&[]int{1, 1, 1, 4, 99, 5, 6, 0, 99}: &[]int{30, 1, 1, 4, 2, 5, 6, 0, 99},
	}

	data2 = map[int]int{
		12:     2,
		14:     2,
		1969:   966,
		100756: 50346,
	}
)

func TestPart1(t *testing.T) {
	for key, expected := range data1 {
		actual := process(*key)
		if !reflect.DeepEqual(actual, *expected) {
			t.Fatalf("part1: expected %d, got %d", expected, actual)
		}
	}
}

// func TestPart2(t *testing.T) {
// 	for i, expected := range data2 {
// 		actual := part2(i)
// 		if actual != expected {
// 			t.Fatalf("part2 %d: expected %d, got %d", i, expected, actual)
// 		}
// 	}
// }
