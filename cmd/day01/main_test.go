package main

import "testing"

var (
	data1 = map[int]int{
		12:     2,
		14:     2,
		1969:   654,
		100756: 33583,
	}

	data2 = map[int]int{
		12:     2,
		14:     2,
		1969:   966,
		100756: 50346,
	}
)

func TestPart1(t *testing.T) {
	for i, expected := range data1 {
		actual := part1(i)
		if actual != expected {
			t.Fatalf("part1 %d: expected %d, got %d", i, expected, actual)
		}
	}
}

func TestPart2(t *testing.T) {
	for i, expected := range data2 {
		actual := part2(i)
		if actual != expected {
			t.Fatalf("part2 %d: expected %d, got %d", i, expected, actual)
		}
	}
}
